$("#sepuluh").mouseover(
	function() {
		$('#satu').css('opacity','1');
		$('#satu').css('transition','1s');
		$('#dua').css('opacity','1');
		$('#dua').css('transition','2s');
		$('#tiga').css('opacity','1');
		$('#tiga').css('transition','4s');
		$('#empat').css('opacity','1');
		$('#empat').css('transition','6s');
		$('#lima').css('opacity','1');
		$('#lima').css('transition','8s');
		$('#enam').css('opacity','1');
		$('#enam').css('transition','10s');
		$('#tujuh').css('opacity','1');
		$('#tujuh').css('transition','12s');
		$('#delapan').css('opacity','1');
		$('#delapan').css('transition','14s');
		$('#sembilan').css('opacity','1');
		$('#sembilan').css('transition','16s');
	}
);
$("#sepuluh").mouseout(
	function() {
		$('#satu').css('opacity','0');
		$('#satu').css('transition','1s');
		$('#dua').css('opacity','0');
		$('#dua').css('transition','1.5s');
		$('#tiga').css('opacity','0');
		$('#tiga').css('transition','2s');
		$('#empat').css('opacity','0');
		$('#empat').css('transition','2.5s');
		$('#lima').css('opacity','0');
		$('#lima').css('transition','3s');
		$('#enam').css('opacity','0');
		$('#enam').css('transition','3.5s');
		$('#tujuh').css('opacity','0');
		$('#tujuh').css('transition','4s');
		$('#delapan').css('opacity','0');
		$('#delapan').css('transition','4.5s');
		$('#sembilan').css('opacity','0');
		$('#sembilan').css('transition','5s');
	}
);

$(document).ready(function() {
var movementStrength = 25;
var height = movementStrength / $(window).height();
var width = movementStrength / $(window).width();
$("#top-image").mousemove(function(e){
          var pageX = e.pageX - ($(window).width() / 2);
          var pageY = e.pageY - ($(window).height() / 2);
          var newvalueX = width * pageX * -1 - 25;
          var newvalueY = height * pageY * -1 - 50;
          $('#top-image').css("background-position", newvalueX+"px     "+newvalueY+"px");
});
});